# Procès verbal du Conseil Exécutif de l'AGEEI du 11 janvier 2019

## Ordre du jour 11-01-2019

1. Ouverture de la réunion
2. Adoption de l’ordre du jour
3. Adoption des minutes du C.E. du 5 décembre 2019
4. Trésorier
    1. Carte de crédit
5. V.-p. interne
    1. Armoire à trophées
    2. Graphiste
6. V.-p. externe
    1. CUSEC
    2. Journée carrière
7. V.-P. aux technologies
    1. Site web
    2. Horaires de cours
    3. Azure
    4. Slack vs. Mattermost
    5. Droit d’accès Google Drive
8. V.-p. compétition
    1. Commandite Red Bull
    2. CS Games
9. V.-p. loisir
    1. Souper de Noël
    2. Party de Noël
    3. Party de mi-session
    4. Cabane à sucre
10. Varia
11. Fermeture
12. Présence

## Ouverture de la réunion

Charles Thérien propose l’ouverture de la réunion. Philippe Van Velzen appuie.
La proposition est adoptée à l’unanimité.

## Adoption de l’ordre du jour

Charles Thérien propose l’adoption de l’ordre du jour. Dalila Boussetta appuie.
La proposition est adoptée à l’unanimité.

## Adoption des minutes du C.E. 5 décembre 2019

Armand Brière propose l’adoption des minutes du C.E. du 05-11-2018. Philippe Van
Velzen appuie. La proposition est adoptée à l’unanimité.

## Trésorier

### Carte de crédit

Charles Thérien propose que le Trésorier (Armand Brière) ait une carte de crédit
de l’association pour payer lors des évènements (sous condition que la
présidente approuve la tenue de compte courant). Dalila Boussetta appuie. La
proposition est adoptée à l’unanimité.

## V.-p. interne

### Armoire à trophées

Les recherches sont toujours en cours. Graphiste On a reçu une candidature
(Corinne Pulgar).

Charles Thérien propose que l’AGEEI contracte Corinne Pulgar pour faire les
affiches pour la journée carrière avec un budget max de 200$ (au taux horaire de
45$/h) (provenant du budget graphiste), sous condition que les droits de
l’affiche (incluant les droits de modifications et d’utilisation future)
appartiennent à l’AGEEI. Dalila Boussetta appuie. La proposition est adoptée à
l'unanimité.

### Journée carrière

ElleCode veulent sponsoriser notre journée carrière (en fournissant deux
bénévoles et 500$) pour que ElleCode ait leur logo inclus dans nos affiches et
qu’ElleCode ait une table.

Dalila Boussetta propose que ElleCode ait une table pendant la journée carrière
en échange qu’ElleCode nous fournissent deux bénévoles pour la journée carrière.
Philippe Van Velzen appuie. La proposition est battue à majorité

Dalila Boussetta propose que l’AGEEI utilise les thermos restant des deux
dernières initiations pour servir le café aux entreprises. Philippe Grégoire
appuie. La proposition est adoptée à l'unanimité.

Alexis B. et Maxime M. s’étaient engagés à faire la diffusion des événements
entourant la journée carrière.

Charles T. s’engage à faire le recrutement de bénévoles pour la journée carrière
(objectif 10 bénévoles).

Marie-Pier s’engage à se procurer le matière pour l’eau et les jus et les nappes
(32).

Deux événements vont précéder la journée carrière:  23 janvier avec Jacques
Berger “Journée Carrière” (écrire un CV) 30 janvier avec le SVE: un atelier de
lecture/correction de CV

Charles s’engage gérer les inscriptions, la pizza et autres.

Dalila Boussetta propose un budget de 200$ pour la pizza pour l’événement du 23
janvier Luis Nobre appuie. La proposition est adoptée à l'unanimité.

## V.-P. aux technologies

### Site web

Armand Brière propose d’organiser une première réunion de planification et une
réunion d’organisation/formation. Philippe Grégoire appuie. La proposition est
adoptée à l'unanimité..

Armand B. s’engage à le faire.

### Horaires de cours

Le scraper de mise à jour en temps réel des offres de cours et des professeurs
devraient être complété pour la session d’été. Azure

### Slack vs. Mattermost

Ça fonctionne, mais le coût de migration est probablement trop élevé.

### Droit d’accès Google Drive

Charles et Armand vont regarder la méthode la plus efficace pour gérer les
droits et la propriété de l’ensemble du Drive.

## V.-p. compétition

### Commandite Red Bull

Charles T. s’engage à communiquer avec Red Bull pour obtenir les conditions de
leurs offres.

### CSG ames

On commence les qualifications pour les CS Games le 14 janvier. On prévoit faire
la première rencontre de préparation pour la dernière semaine de janvier. On a
un parrain, Marc-Antoine Sauvé.

## V.-p. loisir

### Souper de Noël

Ça a coûté 1903.99$ Post-mortem: Réservation devrait être faite beaucoup plus
tôt dans la saison (idéalement en septembre), parce que sinon tout est pris.
Événement très apprécié des étudiants, excellent feedback des étudiants.

### Party de Noël

On a pas encore les coûts finaux. Post-mortem: soirée remplie de problèmes. Les
vestiaires ont été un désastre. La sécurité a dû rester 2h supplémentaires. Il y
a eu du vandalisme et de la casse.  L’AEMA n’a pas été un bon partenaire.
L’AGEEI devrait s’assurer d’avoir des pouvoirs décisionnels égaux avec ses
partenaires.

### Party de mi-session

Une soirée sans partenaire maxime.

Communiquer avec le BIPH si on organise ça à l’UQAM pour s’assurer que GARDA
remplissent bien leurs responsabilités.

### Cabane à sucre

Idéalement on voudrait une cabane à sucre avec option végane, et qui nous laisse
contrôler la musique.

Luis Nobre va contacter d’autres associations pour y aller en partenariat.

## Varia

Le colis contenant l'encre jaune pour l'imprimante s'est encore perdu en chemin
(listé sous "livré" mais personne ne l'a).

## Fermeture

Charles Thérien propose la fermeture de la réunion. Luis Nobre appuie. La
proposition est adoptée à l’unanimité.

## Présence

- Armand Brière
- Charles Thérien
- Dalila Boussetta
- Luis Nobre
- Marie-Pier Lessard
- Philippe Grégoire
- Philippe Van Velzen
