# Réunion du 20 septembre 2018

## Ordre du jour 2018-09-20

1. Ouverture
2. Adoption de l’ordre du jour
3. Adoption du pv
   1. P.V. du 19 juillet 2018
4. Assemblée générale
5. V.-P. interne
   1. Comité de programme
6. V.-P. techno
   1. Azure
   2. GitLab
   3. _Scraper_ pour horaire
7. V.-P. externe
   1. Redéfinition de V.-P. externe
   2. Définition du poste "representant premiere annee"
   3. Définition du poste "adjoint"
8. V.-P. compétitions
   1. Comité compétitions
9. Trésorier
   1. Affiche pour compétitions
10. V.-P. loisirs
11. Varia
12. Fermeture

## 1. Ouverture

Charles Thérien propose l'ouverture de la réunion.
Phil appuie.
La proposition est adoptée à l’unanimité.

## 2. Adoption de l’ordre du jour

Charles Thérien propose l'adoption de l'ordre du jour.
Francis Pelletier appuie.
La proposition est adoptée à l’unanimité.

## 3. Adoption du procès verbal du 19 juillet 2018

Philipe propose l’adoption du P.V. sous condition d’y ajouter le tableur
de budget prévisionnel détaillé de compétition.
Charles Thérien appuie.
La proposition est adoptée à l’unanimité.

## 4. Assemblée générale

Charles Thérien propose de tenir une assemblée générale le 3 octobre à 12:30.
Francis Pelletier appuie.
La proposition est adopté à l’unanimité.

## 5. V.-P. interne

### 5.1. Comité de programme

Le processus de sélection n'a pas été optimal cette année: premier
arrivé, seulement par Slack. Il faudra faire mieux l'année prochaine.

## 6. V.-P. aux technologies

### 6.1. Azure

Une inscription comme OBNL auprès de microsoft donne droit à Office365
et à un crédit annuel de 5000$ pour les services Azure. Cela permettrait
d’avoir une meilleure disponibilité puisque l'UQAM coupe régulièrement
le courant des serveurs sans annonces.

Philippe Grégoire propose de s’inscrire sur Azure.
Charles Thérien appuie.
La proposition est adoptée à l’unanimité.

### 6.1. GitLab

La migration est faite pour tout sauf le site web.

### 6.2. Scraper pour horaire

Philippe Grégoire propose que l’AGEEI prenne la responsabilité de ce service.
Luis Nobre appuie.
La proposition est adoptée à l’unanimité.

## 7. V.-P. externe

### 7.1. Redéfinition de la description du poste

"Le vice-président aux affaires externes s'assure des relations externes de
l'AGEEI, soit envers l'AESS, ses associations modulaires, les autres
associations et instances de l'UQAM et des autres universités, ainsi que les
entreprises et autres organismes, selon les besoins. Il a pour mandat de gérer
l’image de l’Association ainsi que de créer des liens avec la communauté
(entreprises, groupes usagers, etc.)."

Définition plus proche de celle de l’AESS
À amener en A.G.

### 7.2. Définition du poste "représentant première année"

"Le poste de représentant.e première année se veut une introduction au conseil
exécutif. Il se veut d'une implication plus flexible, mais surtout d'une
représentation au conseil de chaque nouvelle cohorte afin de favoriser le
contact et l'intégration de celle-ci au reste des étudiant.e.s de l'AGEEI.

Pour appliquer à ce poste, l'étudiant.e doit en être au plus à sa deuxième
session lors de sa première session d'automne et conséquemment, peut seulement
être élu.e pendant cette session. Un maximum d'un seul mandat d’un an peut être
rempli par un.e même étudiant.e, à la fin duquel la personne est libre (et
fortement encouragée) d'appliquer à un autre poste du conseil exécutif.

Ce poste comporte les mêmes responsabilités et pouvoirs que les autres
officier.e.s de l'AGEEI."

#### Points soulevés

- responsabilité non-explicitée
- officier?
- assistant... adjoints?
- séparation de la représentation apparente? (Officier de première année?)

À amener en A.G.

### 7.3. Définition du poste "adjoint"

"Chacun des postes ci-dessus, à l'exception de représentant.e première année,
peuvent se doter d'un.e adjoint.e. Les adjoints ne possèdent aucun pouvoir ni
responsabilité et ne sont pas des officier.e.s de l'AGEEI. Les adjoint.e.s n'ont
pas de droit de vote lors des conseils exécutifs, outre que s'ils sont désignés
pour remplacer leur supérieur lors de leur absence à un conseil exécutif."

À amener en A.G.

## 8. V.-P. aux compétitions

### 8.1. comité compétitions

[fichier
descripteur](https://docs.google.com/document/d/1stF9uB_tC4I4XWhd4eSOHTy39CTYMUon61APJ9MWAHk/edit?usp=sharing)
C'est une bonne idée, le V.-P. aux compétitions va tenter de mettre en place ce
comité informellement, et si ce comité a suffisamment de traction, nous pourrons
le formaliser.

## 9. Trésorier

### 9.1. Affiche pour compétition

Il faut faire le suivi.
Francis Pelletier propose le choix de Sébastien Dorion pour faire l’affiche.
Philippe Grégoire appuie.
La proposition est adoptée à l’unanimité.

Philippe Van Velzen est en charge de faire contact et de faire le suivi.
(contact établi d’ici le 4 octobre, avec design prêt le 16 octobre)

## 10. V.-P. aux loisirs

Halloween: contacter l’AESS

## 11. Varia

## 12. Fermeture

Charles Thérien propose la fermeture de la réunion.
Francis Pelletier appuie.
La proposition est adoptée à l’unanimité.
