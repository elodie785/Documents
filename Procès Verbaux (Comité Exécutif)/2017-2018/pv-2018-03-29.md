# Réunion du 29 mars 2018

## Ordre du jour 2018-03-29

1. Ouverture
2. Adoption de l'ordre du jour
3. Adoption des procès-verbaux
4. Technologie
  4.1. Achat de matériel
5. Compétition
  5.1. Northsec
  5.2. CS games
6. Externe
  6.1. Retour sur la journée carrière
7. Interne
8. Loisir
  8.1. Cabane à sucre
9. Budget
  9.1. Énumération des dépenses à venir
10. Fermeture

## 1. Ouverture

Vincent Dansereau propose l'ouverture de la réunion.
Koffi Ballo appuie.
La proposition est adoptée à l'unanimité.

## 2. Adoption de l'ordre du jour

Vincent Dansereau propose l'adoption de l'ordre du jour.
Francis Pelletier appuie.
La proposition est adoptée à l'unanimité.

## 3. Adoption des procès-verbaux

Philippe Grégoire propose l'adoption du procès-verbal 15 février 2018.
Francis Pelletier appuie.
La proposition est adoptée à l'unanimité.

## 4. Technologie

### 4.1. Achat de matériel

Le matériel va être acheté avec l'argent qui avait
été budgété dans une rencontre précédente.

## 5. Compétition

## 5.1. Northsec

Desjardins offre une commandite de 500$.
La compagnie où travail Ricardo offre un don. À déterminer.
5 billets de conférences à acheter éventuellement.
Il y a 13 personnes qui ont rempli le questionnaire d'intérêt.
Il y aura 8 personnes sélectionnées.
Plénière de 15 minutes sur l'acceptation des commandites.
Nicolas Lamoureux propose qu'on alloue un montant de 1 200$ pour acheter des
billets de conférences pour le Northsec.
Francis Pelletier appuie.
La proposition est adoptée à l'unanimité.
Nicolas Lamoureux propose qu'on accepte la commandite de 500$ de Desjardins.
Philippe Grégoire appuie.
La proposition est adoptée à l'unanimité.
Le montant de Desjardins et le surplus d'argent budgété pour les CS games
seront utilisés pour la fabrication de chandails et le graphisme de ceux-ci.
Vincent Dansereau propose qu'on fasse une sélection de 6 personnes par
qualification et une sélection de 2 personnes au hasard pour
les équipes du Northsec.
Koffi Ballo propose un amendement à la proposition.
Il propose qu'on fasse la sélection des 8 personnes par qualification
pour les équipes du Northsec.
Nicolas Lamoureux appuie.
La proposition amendée est adoptée à majorité.

## 5.2. CS games

Armoire pour trophées des CS games.

## 6. Externe

### 6.1. Retour sur la journée carrière

Changement de local effectué à la dernière minute.

Bilan de la journée carrière :

* Leur date idéale début/mi-février.
* Le local était isolé et éloigné.
* Contact de qualité avec les étudiants.
* Faire le suivi, encaisser les chèques.
* À améliorer : Contact avec les recruteurs.

## 7. Interne

L'occupant du poste est destitué.

## 8. Loisir

### 8.1. Cabane à sucre

Aura lieu demain le vendredi 30 mars 2018.

## 9. Budget

### 9.1. Énumération des dépenses à venir

Voici les dépenses à venir:

* Cabane à sucre
* Pizza Midi-Conférence
* Northsec

## 10. Fermeture

Nicolas Lamoureux propose la fermeture de la réunion.
Vincent Dansereau appuie.
La proposition est adoptée à l'unanimité.

## Liste des présences

* Nicolas Lamoureux
* Philippe Grégoire
* Francis Pelletier
* Vincent Dansereau
* Koffi Ballo
* Marie-Pier Lessard
* Marc-André Labelle
